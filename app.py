###############################################
# Import packages
###############################################
from flask import Flask, render_template, url_for, request, redirect
import requests
import re
import json
import os
import xml.etree.ElementTree as ElementTree

###############################################
# Define flask app
###############################################
app = Flask(__name__)
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))


###############################################
# XML Parsing
###############################################
def get_properties(tag):
    tag_properties = {}

    for properties in tag:

        if properties.tag == 'WritKey':

            key = ''
            if properties.text.split(':')[0] == 'Major':
                key = properties.text.split(':')[1]
            if properties.text.split(':')[0] == 'Minor':
                key = properties.text.split(':')[1] + " min"
            tag_properties['WritKey'] = key

        elif properties.tag == 'Rating':
            tag_properties['Rating'] = round(float(properties.text), 2)

        elif properties.tag == 'videos':
            tag_properties['videos'] = properties.attrib['available']

        else:
            tag_properties[properties.tag] = properties.text

    return tag_properties


###############################################
# Home page
###############################################
@app.route('/')
def home():
    json_url = os.path.join(SITE_ROOT, 'static/assets/json/tag_data/68.json')
    tag_data = json.load(open(json_url))
    return render_template('index.html', tag_data=tag_data)


###############################################
# Browse page
###############################################
@app.route('/browse', methods=['POST', 'GET'])
def browse_init():
    json_url = os.path.join(SITE_ROOT, 'static/assets/json/tag_data.json')
    tag_data = json.load(open(json_url))

    # Redirect
    if request.method == 'POST':
        keywords = request.form['search-box']
        return redirect(url_for('browse', search_criteria=keywords))
    else:
        return render_template('browse.html', results=tag_data)


###############################################
# Browse page with search criteria
###############################################
@app.route('/browse/<search_criteria>', methods=['POST', 'GET'])
def browse(search_criteria):
    json_url = os.path.join(SITE_ROOT, 'static/assets/json/tag_data.json')
    tag_data = json.load(open(json_url))

    results = [tag for tag in tag_data if search_criteria.lower() in tag['Title'].lower()]

    # Redirect
    if request.method == 'POST':
        keywords = request.form['search-box']
        return redirect(url_for('browse', search_criteria=keywords))
    else:
        return render_template('browse.html', results=results)


###############################################
# Visualiser Page
###############################################
@app.route('/visualiser/<tag_id>', methods=['POST', 'GET'])
def visualiser(tag_id):
    json_url = os.path.join(SITE_ROOT, 'static/assets/json/tag_data/' + tag_id + '.json')
    tag_data = json.load(open(json_url))
    return render_template('visualiser.html', tag_data=tag_data)


###############################################
# Main driver
###############################################
if __name__ == '__main__':
    app.run(debug=True)
