/**************************************************************
 * The Visualiser Represented by a LineChart
 * @param input
 * @constructor
 **************************************************************/
var LineChart = function( input ) {

  var bass_data = input.bass_data;
  var bari_data = input.bari_data;
  var lead_data = input.lead_data;
  var tenor_data = input.tenor_data;

  var canvas = document.getElementById('visualiser');
  var context = canvas.getContext( '2d' );

  var rendering = false,
      width = ( input.width || window.innerWidth ) * 2,
      height = ( input.width || window.innerWidth ),
      paddingX = width * 0.06,
      paddingY = height * 0.02,
      progress = 0;

  canvas.width = width;
  canvas.height = height;

  format();
  render();

  /**************************************************************
   * Set the position values for the data points
   * @param force
   **************************************************************/
  function format(force) {
    format_part(bass_data, force);
    format_part(bari_data, force);
    format_part(lead_data, force);
    format_part(tenor_data, force);
  }

  function format_part(data, force) {

    data.forEach(function (point, i) {
      point.targetX = paddingX + (i / (data.length - 1)) * (width - (paddingX * 2));
      point.targetY = paddingY + ((point.value - minValue) / (maxValue - minValue) * (height - (paddingY * 2)));
      point.targetY = height - point.targetY;

      if ( force || (!point.x && !point.y)) {
        point.x = point.targetX + width * 0.011;
        point.y = point.targetY;
        point.speed = 0.04 + (1 - (i / data.length)) * 0.05;
      }
    });
  }

  /**************************************************************
   * Render the scene
   **************************************************************/
  function render() {

    document.getElementById('progress-bar').value = progress;

    if( !rendering ) {
      requestAnimationFrame( render );
      return;
    }

    context.font = '20px sans-serif';
    context.clearRect( 0, 0, width, height );

    render_part(bass_data);
    render_part(bari_data);
    render_part(lead_data);
    render_part(tenor_data);

    progress += 1.0 / 60 / duration;

    requestAnimationFrame( render );
  }

  function render_part(data) {

    var progressDots = Math.floor( progress * data.length );

    for (let i = 0; i < data.length; i++) {

      if( i <= progressDots && i > progressDots - 100 ) {
        data[i].x += ( data[i].targetX - data[i].x ) * data[i].speed;
        data[i].y += ( data[i].targetY - data[i].y ) * data[i].speed;

        if( i < progressDots) {
          context.beginPath();
          context.arc( data[i].x, data[i].y, width * 0.012, 0, Math.PI * 2 );
          context.fillStyle = data[i].color;
          context.globalAlpha = (data[i].x - data[i].targetX) / (width * 0.011);
          context.fill();
        }
      }

    }
  }

  /***************************************************************
   * Utility functions
   **************************************************************/
  this.restart = function() {
    progress = 0;
    stop();
    format( true );
    start();
  }

  this.start = function() {
    rendering = true;
  }

  this.stop = function() {
    rendering = false;
  }

  this.populate = function( bs, br, ld, tr ) {
    progress = 0;
    bass_data = bs;
    bari_data = br;
    lead_data = ld;
    tenor_data = tr;

    format();
  }

  this.setProgress = function( input ) {
    progress = input;
    format( true );
    start();
  }

};


/**************************************************************
 * Initialisation, read data
 **************************************************************/
var chart = new LineChart({ bass_data: [], bari_data: [], lead_data: [], tenor_data: [] });
var audio_ended = false;
chart.populate(input_bass_data, input_bari_data, input_lead_data, input_tenor_data);


/**************************************************************
 * Global behavioral functions
 **************************************************************/
function restart() {
  document.getElementById("audio").currentTime = 0;
  document.getElementById("audio").play();
  chart.restart();
}

function start() {
  if ( audio_ended ) {
    audio_ended = false;
    restart();
  } else {
    document.getElementById("audio").play();
    chart.start();
  }
}

function stop() {
  document.getElementById("audio").pause();
  chart.stop();
}

document.getElementById("audio").addEventListener("ended", function(){
  audio_ended = true;
});

document.getElementById('progress-bar').addEventListener('click', function (e) {

  let clickPosition = e.offsetX * this.max / this.offsetWidth;

  let aud = document.getElementById('audio');
  aud.currentTime = clickPosition * aud.duration;
  chart.setProgress(clickPosition);
});

document.addEventListener("visibilitychange", stop);